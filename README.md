# GitLab CI Parallel Matrix Artifacts

It’s not possible (yet) to use those matrix values to specify the job name inside the dependencies block.

The “solution” I found was creating the artifacts inside the same parent directory but with a different path/name for each job.

Because we can define an `environment:name` using the parallel matrix variables, I can create an artifact with a different name on each job using the pre-defined `CI_ENVIRONMENT_NAME` variable.

More details inside the `.gitlab-ci.yml` file.

---

Link to the open issue: https://gitlab.com/gitlab-org/gitlab/-/issues/396845
